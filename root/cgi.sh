#!/bin/sh

if [ "$QUERY_STRING" = "cert" ] && [ -z "$TLS_CLIENT_HASH" ]; then
    echo "60 Please supply a client certificate"
    exit 0
fi

echo "20 text/gemini"

cat <<EOF
# CGI Example

General variables:

* DOCUMENT_ROOT
  $DOCUMENT_ROOT

* GATEWAY_INTERFACE
  $GATEWAY_INTERFACE

* PATH_INFO
  $PATH_INFO

* PATH_TRANSLATED
  $PATH_TRANSLATED

* QUERY_STRING
  $QUERY_STRING

* SCRIPT_NAME
  $SCRIPT_NAME

* SERVER_NAME
  $SERVER_NAME

* SERVER_PROTOCOL
  $SERVER_PROTOCOL

* SERVER_SOFTWARE
  $SERVER_SOFTWARE

Gemini specific:

* GEMINI_URL
  $GEMINI_URL

EOF

if [ -z "$TLS_CLIENT_HASH" ]; then
    echo "=> /$SCRIPT_NAME?cert Request a client certificate"
else
    cat <<EOF
Client certificate:

* AUTH_TYPE
  $AUTH_TYPE

* REMOTE_USER
  $REMOTE_USER

* TLS_CLIENT_HASH
  $TLS_CLIENT_HASH
EOF
fi
