#lang racket

(provide wrap)

(define (wrap handler)
  (lambda (req)
    (call-with-values
     (lambda () (handler req))
     (case-lambda
       [(status meta body)
        (if (string-prefix? meta "text/")
            (values status meta
                    (string-replace (if (string? body)
                                        body
                                        (read-string 65536 body))
                                    "http"
                                    "that-other-protocol"))
            (values status meta body))]
       [res
        (apply values res)]))))
