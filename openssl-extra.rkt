#lang racket/base

(require (rename-in racket/contract/base [-> c->])
         ffi/unsafe
         ffi/unsafe/define
         openssl
         openssl/libcrypto
         openssl/libssl
         openssl/sha1
         rackunit
         racket/list)

(provide (all-from-out openssl)
         ssl-port->certificate
         (struct-out certificate)
         (contract-out
          [ssl-set-verify-callback!
           (c-> ssl-server-context?
                (c-> boolean? certificate? boolean?)
                procedure?)]))

;; break open mzssl
(require/expose openssl/mzssl (ssl-context-ctx
                               ssl-port->cert))

(define-cpointer-type _SSL*)
(define-cpointer-type _SSL_CTX*)
(define-cpointer-type _X509*)
(define-cpointer-type _X509_STORE_CTX*)
(define-cpointer-type _X509_NAME*)
(define-cpointer-type _X509_NAME_ENTRY*)
(define-cpointer-type _EVP_MD*)
(define-cpointer-type _int*)
(define-cpointer-type _ASN1_OBJECT*)
(define-cpointer-type _ASN1_STRING*)

(define-ffi-definer define-crypto libcrypto
  #:default-make-fail make-not-available)
(define-ffi-definer define-ssl libssl
  #:default-make-fail make-not-available)

(define SSL_VERIFY_PEER #x01)
(define SSL_VERIFY_CLIENT_ONCE #x04)

(define-crypto EVP_get_digestbyname (_fun _string -> _EVP_MD*))
(define-crypto X509_digest (_fun _X509* _EVP_MD* _bytes _pointer -> _int))
(define-crypto X509_get_subject_name (_fun _X509* -> _X509_NAME*))
(define-crypto X509_get_issuer_name (_fun _X509* -> _X509_NAME*))
(define-crypto X509_NAME_entry_count (_fun _X509_NAME* -> _int))
(define-crypto X509_NAME_get_entry (_fun _X509_NAME* _int -> _X509_NAME_ENTRY*))
(define-crypto X509_NAME_ENTRY_get_object (_fun _X509_NAME_ENTRY* -> _ASN1_OBJECT*))
(define-crypto X509_NAME_ENTRY_get_data (_fun _X509_NAME_ENTRY* -> _ASN1_STRING*))
(define-crypto X509_STORE_CTX_get_current_cert (_fun _X509_STORE_CTX* -> _X509*))
(define-crypto ASN1_STRING_length (_fun _ASN1_STRING* -> _int))
(define-crypto ASN1_STRING_data (_fun _ASN1_STRING* -> _pointer))
(define-crypto OBJ_obj2txt (_fun _bytes _int _ASN1_OBJECT* _int -> _int))
;;  typedef int (*SSL_verify_cb)(int preverify_ok, X509_STORE_CTX *x509_ctx);
;;  void SSL_CTX_set_verify(SSL_CTX *ctx, int mode, SSL_verify_cb verify_callback);
(define-ssl SSL_CTX_set_verify (_fun _SSL_CTX* _int (_fun _int _X509_STORE_CTX* -> _int) -> _void))

(struct certificate (subject-name issuer-name digest-sha256))

(define (asn1obj->str asn1obj)
  (let* ([buf (make-bytes 80 0)]
         [len (OBJ_obj2txt buf (bytes-length buf) asn1obj 0)])
    (bytes->string/utf-8 buf #f 0 len)))

(define (asn1str->str asn1str)
  (let* ([len (ASN1_STRING_length asn1str)]
         [data (ASN1_STRING_data asn1str)]
         [buf (make-bytes len 0)])
    (memcpy buf data len)
    (bytes->string/utf-8 buf)))

(define (x509-name->hash x509-name)
  (for/hash ([loc (range 0 (X509_NAME_entry_count x509-name))])
    (let ([entry (X509_NAME_get_entry x509-name loc)])
      (values (asn1obj->str (X509_NAME_ENTRY_get_object entry))
              (asn1str->str (X509_NAME_ENTRY_get_data entry))))))

(define (x509-store-ctx->x509 ctx)
  (let* ([x509 (X509_STORE_CTX_get_current_cert ctx)]
         [digest-bytes (make-bytes 32 0)])
    (X509_digest x509 (EVP_get_digestbyname "sha256") digest-bytes #f)
    (certificate (x509-name->hash (X509_get_subject_name x509))
                 (x509-name->hash (X509_get_issuer_name x509))
                 (bytes->hex-string digest-bytes))))

;; The callback accepts to arguments: preverify-ok and cert, and it
;; returns #t if the certificate is accepted otherwise #f.  Returns
;; the actual callback which needs to be kept save from garbage
;; collection.
(define (ssl-set-verify-callback! ssl-context callback)
  (let ([ctx (ssl-context-ctx ssl-context)]
        [cb (lambda (preverify-ok peer-ctx)
              (if (callback (if (eq? preverify-ok 1) #t #f)
                            (x509-store-ctx->x509 peer-ctx))
                  1
                  0))])
    (SSL_CTX_set_verify ctx
                        (bitwise-ior SSL_VERIFY_PEER SSL_VERIFY_CLIENT_ONCE)
                        cb)
    cb))

(define (x509->certificate x509)
  (let ([digest-bytes (make-bytes 32 0)])
    (X509_digest x509 (EVP_get_digestbyname "sha256") digest-bytes #f)
    (certificate (x509-name->hash (X509_get_subject_name x509))
                 (x509-name->hash (X509_get_issuer_name x509))
                 (bytes->hex-string digest-bytes))))

(define (ssl-port->certificate ssl-port)
  ;; ssl-port->cert return x509 C object
  (let ([x509 (ssl-port->cert 'ssl-port->certificate ssl-port)])
    (if x509
        (x509->certificate x509)
        #f)))
